// app/models/hero.js

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var HeroSchema   = new Schema({
	_id: String,
    name: String
});

module.exports = mongoose.model('Hero', HeroSchema);