// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var cors       = require('cors');
var bodyParser = require('body-parser');
var Hero       = require('./app/models/hero');
var _          = require('lodash');
var Q          = require('q');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	res.header('Access-Control-Allow-Headers', 'Content-Type');
    next(); // make sure we go to the next routes and don't stop here
});


// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });   
});

// more routes for our API will happen here

// on routes that end in /heroes
// ----------------------------------------------------
router.route('/heroes')

    // create a hero (accessed at POST http://localhost:8080/api/heroes)
    .post(function(req, res) {
        
        var hero = new Hero();      // create a new instance of the Hero model
        hero.name = req.body.name;  // set the heroes name (comes from the request)
		hero._id = req.body._id;

        // save the hero and check for errors
        hero.save(function(err) {
            if (err) {
				res.send(err);
			} else {
				res.json({ 
					_id : hero._id,
					name: hero.name
				});
			}
        });
        
    })
	
	// get all the heroes (accessed at GET http://localhost:8080/api/heroes)
    .get(function(req, res) {
        Hero.find(function(err, heroes) {
            if (err)
                res.send(err);

            res.json(heroes);
        });
    })
	
	// delete all hereos (accessed at DELETE http://localhost:8080/api/heroes)
	.delete(function(req, res) {
        mongoose.connection.db.dropCollection('heros', function(err, result) {
			if (err) {
				res.send(err);
			} else {
				res.json({ message: 'Heroes deleted!' });
			}
		});
    })
	
	// Empty heroes then Init default heroes
	.purge(function(req, res) {
		mongoose.connection.db.dropCollection('heros', function(err, result) {			
			var heroes = [
			    {_id: 'Mr.Nice', name: 'Mr. Nice'},
			    {_id: 'Narco', name: 'Narco'},
			    {_id: 'Bombasto', name: 'Bombasto'},
			    {_id: 'Celeritas', name: 'Celeritas'},
			    {_id: 'Magneta', name: 'Magneta'},
			    {_id: 'RubberMan', name: 'RubberMan'},
			    {_id: 'Dynama', name: 'Dynama'},
			    {_id: 'DrIQ', name: 'Dr IQ'},
			    {_id: 'Magma', name: 'Magma'},
			    {_id: 'Tornado', name: 'Tornado'}
			];
			var promises = [];
			_.forEach(heroes, function(myHero) {			    
				promises.push(saveToBDD(myHero));
			});
			Q.all(promises).then(function(){
				res.json({ message: 'Heroes initialized!' });
			});			
		});
	})
	
// on routes that end in /heroes/:hero_id
// ----------------------------------------------------	
	// get the hero with that id (accessed at GET http://localhost:8080/api/heroes/:hero_id)
router.route('/heroes/:hero_id')
    .get(function(req, res) {
        Hero.findById(req.params.hero_id, function(err, hero) {
            if (err)
                res.send(err);
            res.json(hero);
        });
    })
	
	// update the hero with this id (accessed at PUT http://localhost:8080/api/heroes/:hero_id)
    .put(function(req, res) {

        // use our hero model to find the hero we want
        Hero.findById(req.params.hero_id, function(err, hero) {

            if (err)
                res.send(err);

            hero.name = req.body.name;  // update the heroes info

            // save the hero
            hero.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'Hero updated!' });
            });

        });
    })
	
	// delete the hero with this id (accessed at DELETE http://localhost:8080/api/heroes/:hero_id)
    .delete(function(req, res) {
        Hero.remove({
            _id: req.params.hero_id
        }, function(err, hero) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });


// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);


// BASE SETUP
// =============================================================================

var mongoose   = require('mongoose');
mongoose.connect('mongodb://127.0.0.1/heroes'); // connect to our database

// Custom save function
function saveToBDD(myHero) {
	var deferred = Q.defer();
	var hero = new Hero();      // create a new instance of the Hero model
	hero.name = myHero.name;  
	hero._id = myHero._id;
	hero.save(function(err) {
		if (err) {
			deferred.reject();
		} else {
			deferred.resolve();
		}
	});
	return deferred.promise;
}
